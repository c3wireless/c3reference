from typing import Any

def new(key: bytes, mode, *args, **kwargs) -> Any:
    ...

MODE_EAX = ... # type: int
